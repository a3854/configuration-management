# Apache on Ubuntu

This playbook will install the Apache 2 web server on an Ubuntu machine. A virtualhost will be created with the options specified in the `vars/default.yml` variable file.

## Settings

- `app_user`: a remote non-root user on the Ansible host that will own the application files.
- `http_host`: your domain name.
- `http_conf`: the name of the configuration file that will be created within Apache.
- `http_port`: HTTP port, default is 80.
- `disable_default`: whether or not to disable the default Apache website. When set to true, your new virtualhost should be used as default website. Default is true.


## Running this Playbook

Quick Steps:

### 1. Put Private Key in VCS
`This is the same key which help to grant SSH access to the destination machine, which can be setup in Profile SSH Settings.` 

### 2. Obtain the playbook
```shell
cd .
```

### 3. Customize Options

```shell
vim vars/default.yml
```

```yml
#vars/default.yml
---
app_user: "ayodele"
http_host: "ayodele.local"
http_conf: "main.conf"
http_port: "80"
disable_default: true
```

### 4. Run the Playbook

```command
ansible-playbook -l [target] -i [inventory file] -u [remote user] playbook.yml
```


